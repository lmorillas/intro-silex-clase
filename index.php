<?php

require_once __DIR__.'/vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = true;

// código de los controladores
 

$app->get('/hola/{nombre}', function($nombre) use($app) { 
    return '<h2>Hola <strong>'.$app->escape($nombre) . '</strong></h2>'; 
});
 
$app->get('/hola', function(){ 
    return '<h1>Hola desde Silex</h1>'; 
}); 
  
 
 
$app->run();
 
 ?>